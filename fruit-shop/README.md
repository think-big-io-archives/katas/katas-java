# Fruit Shop Kata

10 minutes par itération

### Itération 0
* Trouver un binôme
* Une machine par binôme
* Langage au choix
* Lecture et écriture depuis l'entrée et la sortie standard

### Itération 1
* Faire une caisse enregistreuse simple pour les clients en magasins
* 3 produit
    * Pommes (1,00€)
    * Bananes (1,50€)
    * Cerises (0,75€)
* 1 article par ligne, afficher le total du panier en centimes après chaque entrée

### Itération 2
* Réduction: pour 2 lots de cerises achetés, on applique 20 centimes de réduction
    
### Itération 3
* Support du format CSV en entrée

### Interruption de l'itération

### Itération 3'
* Support du format CSV reporté, retour à une entrée par ligne
* La réduction pour les cerises passe à 30 centimes
* Un lot de banane acheté, le second est offert

### Itération 4
* Support de la localisation: on accepte les mots Apples et Mele pour Pommes
* La réduction pour les cerises repasse à 20 centimes

### Itération 5
* Support du CSV pour la prochaine itération
* 3 lots de Apples pour 2,00€
* 2 lots de Mele pour 1,50€

### Interruption de l'itération

### Itération 5'
* Nouveau hardware, il faut redéployer ASAP
* 3 lots de Apples pour 2,00€
* 2 lots de Mele pour 1,00€

### Itération 6
* 4 pommes achetées, 1,00€ de réduction sur la facture globale
* 5 fruits achetées, 2,00€ de réduction

### Conclusion
* Qui a utilisé un système de gestion de version
* Qui a utilisé des tests, à partir de quelle itération ?
* Qui a écrit et maintenu des tests d'acceptance ?
* Quid du code CSV lorsqu'il n'était plus nécessaire ?
* Est-ce que votre code est propre ?
* Est-ce que votre code répond au besoin ?

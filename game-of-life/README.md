# Game of Life Kata

### Description

This kata is about calculating the next generation of Conway's game of life, given any starting position.

You start with a two dimensional grid of cells, where each cell is either alive or dead.
You should write a program that can accept an arbitrary grid of cells, and will output a similar grid showing the next generation.

When calculating the next generation of the grid, follow these rules:
* Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
* Any live cell with more than three live neighbours dies, as if by overcrowding.
* Any live cell with two or three live neighbours lives on to the next generation.
* Any dead cell with exactly three live neighbours becomes a live cell. 

### Example

The input starting position could be a text file that looks like this:
<pre>
Generation 1:
4 8
........
....*...
...**...
........
</pre>

And the output could look like this:
<pre>
Generation 2:
4 8
........
...**...
...**...
........
</pre>

### Variations

* Finite grid with fix dimension
* Finite grid with dimension given as input on first line, or derived from given grid
* Infinite grid

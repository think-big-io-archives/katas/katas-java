# Java 8 Code Kata
[Github repository](https://github.com/konohiroaki/java8-code-kata)

## Introduction

The Java 8 Code Kata is created to walk-through java8 new API functions.
I hope this helps you learn Java8 and get used to it.
Repeating exercises will definitely upgrade your skills.

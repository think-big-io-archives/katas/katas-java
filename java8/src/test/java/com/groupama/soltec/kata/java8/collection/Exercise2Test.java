package com.groupama.soltec.kata.java8.collection;

import com.groupama.soltec.kata.java8.annotation.Easy;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

import static org.assertj.core.api.Assertions.assertThat;

public class Exercise2Test {

    private final Map<String, Integer> map = new HashMap<String, Integer>() {{
        put("Joe", 22);
        put("Steven", 27);
        put("Patrick", 28);
        put("Chris", 26);
    }};

    @Easy
    @Test
    public void getDefaultValue() {
        Map<String, Integer> map = new HashMap<>(this.map);

        /**
         * Try to get from key "Alice" using {@link Map#getOrDefault}. If the key doesn't exist, use 30 as default.
         */
        Integer defaultVal = null;

        assertThat(defaultVal).isEqualTo(30);
    }

    @Easy
    @Test
    public void putIfNotExisting() {
        Map<String, Integer> map = new HashMap<>(this.map);

        /**
         * Try to put 2 entry with key as "Alice" value as 32, key as "Joe" and value as 32 using {@link Map#putIfAbsent}.
         */
        // map.
        // map.

        assertThat(map.get("Alice")).isEqualTo(32);
        assertThat(map.get("Joe")).isEqualTo(22);
    }

    @Easy
    @Test
    public void mergeValues() {
        Map<String, Integer> map = new HashMap<>(this.map);

        /**
         * Merge 2 entry to {@link map} with key="Alice" value=32, key="Joe" value=32 using {@link Map#merge}.
         * If the value already exist for the key, remap with sum value.
         */
        BiFunction<Object, Object, Integer> remappingFunction = null;
        // map.
        // map.

        assertThat(map.get("Alice")).isEqualTo(32);
        assertThat(map.get("Joe")).isEqualTo(54);
    }

    @Easy
    @Test
    public void ignoringAbsentKeys() {
        Map<String, Integer> map = new HashMap<>(this.map);

        /**
         * Try to increment the value for keys "Joe", "Steven" and "Alice" using {@link Map#computeIfPresent}.
         */
        BiFunction<Object, Object, Integer> remappingFunction = null;
        // map.
        // map.
        // map.

        assertThat(map.get("Joe")).isEqualTo(23);
        assertThat(map.get("Steven")).isEqualTo(28);
        assertThat(map).doesNotContainKey("Alice");
    }
}

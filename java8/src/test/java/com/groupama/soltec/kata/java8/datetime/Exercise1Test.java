package com.groupama.soltec.kata.java8.datetime;

import com.groupama.soltec.kata.java8.annotation.Easy;
import com.groupama.soltec.kata.java8.dataset.DateAndTimes;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;

import static org.assertj.core.api.Assertions.assertThat;

public class Exercise1Test {

    @Easy
    @Test
    public void localDateOf() {
        /**
         * Create a {@link LocalDate} of 2015-06-18 by using {@link LocalDate#of}
         */
        LocalDate localDate = null;

        assertThat(localDate.toString()).isEqualTo("2015-06-18");
    }

    @Easy
    @Test
    public void localDateParse() {
        /**
         * Create a {@link LocalDate} of 2015-06-18 from String by using {@link LocalDate#parse}
         */
        LocalDate localDate = null;

        assertThat(localDate.toString()).isEqualTo("2015-06-18");
    }

    @Easy
    @Test
    public void localDateWith() {
        LocalDate ld = DateAndTimes.LD_20150618;

        /**
         * Create a {@link LocalDate} from {@link ld} with year 2015
         * by using {@link LocalDate#withYear} or {@link LocalDate#with}
         */
        LocalDate localDate = null;

        assertThat(localDate.getYear()).isEqualTo(2015);
        assertThat(localDate.getMonth()).isEqualTo(ld.getMonth());
        assertThat(localDate.getDayOfMonth()).isEqualTo(ld.getDayOfMonth());
    }

    @Easy
    @Test
    public void localDateWithAdjuster() {
        LocalDate ld = DateAndTimes.LD_20150618;

        /**
         * Create a {@link LocalDate} from {@link ld} adjusted into first day of next year
         * by using {@link LocalDate#with} and {@link TemporalAdjusters#firstDayOfNextYear}
         */
        LocalDate localDate = null;

        assertThat(localDate.getYear()).isEqualTo(ld.getYear() + 1);
        assertThat(localDate.getMonth()).isEqualTo(Month.JANUARY);
        assertThat(localDate.getDayOfMonth()).isEqualTo(1);
    }

    @Easy
    @Test
    public void localDatePlus() {
        LocalDate ld = DateAndTimes.LD_20150618;

        /**
         * Create a {@link LocalDate} from {@link ld} with 10 month later
         * by using {@link LocalDate#plusMonths} or {@link LocalDate#plus}
         */
        LocalDate localDate = null;

        assertThat(localDate.getYear()).isEqualTo(ld.getYear() + 1);
        assertThat(localDate.getMonth()).isEqualTo(Month.APRIL);
        assertThat(localDate.getDayOfMonth()).isEqualTo(ld.getDayOfMonth());
    }

    @Easy
    @Test
    public void localDateMinus() {
        LocalDate ld = DateAndTimes.LD_20150618;

        /**
         * Create a {@link LocalDate} from {@link ld} with 10 days before
         * by using {@link LocalDate#minusDays} or {@link LocalDate#minus}
         */
        LocalDate localDate = null;

        assertThat(localDate.getYear()).isEqualTo(ld.getYear());
        assertThat(localDate.getMonth()).isEqualTo(ld.getMonth());
        assertThat(localDate.getDayOfMonth()).isEqualTo(ld.getDayOfMonth() - 10);
    }


    @Easy
    @Test
    public void localDatePlusPeriod() {
        LocalDate ld = DateAndTimes.LD_20150618;

        /**
         * Define a {@link Period} of 1 year 2 month 3 days
         * Create a {@link LocalDate} adding the period to {@link ld} by using {@link LocalDate#plus}
         */
        Period period = null;
        LocalDate localDate = null;

        assertThat(localDate.getYear()).isEqualTo(ld.getYear() + 1);
        assertThat(period.getMonths()).isEqualTo(2);
        assertThat(localDate.getDayOfMonth()).isEqualTo(ld.getDayOfMonth() + 3);
    }

    @Easy
    @Test
    public void localDateIsAfter() {
        LocalDate ld = DateAndTimes.LD_20150618;
        LocalDate ld2 = DateAndTimes.LD_20150807;

        /**
         * Check whether {@link ld2} is after {@link ld} or not
         * by using {@link LocalDate#isAfter} or {@link LocalDate#isBefore}
         */
        boolean isAfter0618 = false;

        assertThat(isAfter0618).isTrue();
    }

    @Easy
    @Test
    public void localDateUntil() {
        LocalDate ld = DateAndTimes.LD_20150618;
        LocalDate ld2 = DateAndTimes.LD_20150807;

        /**
         * Create a period from {@link ld} till {@link ld2}
         * by using {@link LocalDate#until}
         */
        Period period = null;

        assertThat(period.getYears()).isEqualTo(0);
        assertThat(period.getMonths()).isEqualTo(1);
        assertThat(period.getDays()).isEqualTo(20);
    }
}

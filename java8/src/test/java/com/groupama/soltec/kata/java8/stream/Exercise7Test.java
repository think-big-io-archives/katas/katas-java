package com.groupama.soltec.kata.java8.stream;

import com.groupama.soltec.kata.java8.annotation.Easy;
import com.groupama.soltec.kata.java8.dataset.ClassicOnlineStore;
import com.groupama.soltec.kata.java8.entity.Customer;
import com.groupama.soltec.kata.java8.entity.Shop;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

import static org.assertj.core.api.Assertions.assertThat;

public class Exercise7Test extends ClassicOnlineStore {

    @Easy
    @Test
    public void averageAge() {
        List<Customer> customerList = this.mall.getCustomerList();

        /**
         * Create {@link IntStream} with customer ages by using {@link Stream#mapToInt}
         * Then calculate the average of ages by using {@link IntStream#average}
         */
        IntStream ageStream = null;
        OptionalDouble average = null;

        assertThat(average.getAsDouble()).isEqualTo(28.7);
    }

    @Easy
    @Test
    public void howMuchToBuyAllItems() {
        List<Shop> shopList = this.mall.getShopList();

        /**
         * Create {@link LongStream} with all items' prices using {@link Stream#mapToLong}
         * Then calculate the sum of prices using {@link LongStream#sum}
         */
        LongStream priceStream = null;
        long priceSum = 0;

        assertThat(priceSum).isEqualTo(60930L);
    }
}

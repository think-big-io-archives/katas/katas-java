package com.groupama.soltec.kata.lambda;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.function.*;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * This set of exercises is about lambdas and method references.
 * You will write a lambda or method reference corresponding to
 * each of several different functional interfaces. Each exercise
 * is named after the functional interface intended to be used
 * as the solution.
 */
public class A_Lamdas {
    /**
     * Write a lambda expression that is a predicate
     * that tests whether a string is longer than four characters.
     */
    @Test
    public void a_predicate1() {
        Predicate<String> pred = null; //TODO

        assertThat(pred.test("abcde")).isTrue();
        assertThat(pred.test("abcd")).isFalse();
    }

    /**
     * Write a lambda expression that is a predicate
     * that tests whether a string is empty.
     */
    @Test
    public void a_predicate2() {
        Predicate<String> pred = null; //TODO

        assertThat(pred.test("")).isTrue();
        assertThat(pred.test("a")).isFalse();
    }

    /**
     * Write an unbound method reference that is a predicate
     * that tests whether a string is empty. An unbound method
     * reference has a class name on the left-hand side of the ::
     * operator:
     * <p>
     * classname::methodname
     */
    @Test
    public void a_predicate3() {
        Predicate<String> pred = null; //TODO

        assertThat(pred.test("")).isTrue();
        assertThat(pred.test("a")).isFalse();
    }
    // Hint:
    // <editor-fold defaultstate="collapsed">
    // Copy the lambda expression from the previous exercise and then pop
    // up the menu over the "light bulb" icon in the left margin. This menu
    // has an option to convert the lambda to a method reference. (The exact
    // gesture will vary among IDEs.)
    // </editor-fold>

    /**
     * Given the predicate p1, write a predicate that
     * returns true if the string it tests is not empty.
     * This is a NOT operation on the predicate p1.
     */
    @Test
    public void a_predicate4() {
        Predicate<String> p1 = s -> s.isEmpty();

        Predicate<String> notPredicate = null; //TODO

        assertThat(notPredicate.test("")).isFalse();
        assertThat(notPredicate.test("Not empty!")).isTrue();
    }

    /**
     * Create a predicate that returns true if both predicates
     * startsWithJ and lengthIs7 hold.
     */
    @Test
    public void a_predicate5() {
        Predicate<String> startsWithJ = s -> s.startsWith("J");
        Predicate<String> lengthIs7 = s -> s.length() == 7;

        Predicate<String> startsWithJAndLengthIs7 = null; //TODO

        assertThat(startsWithJAndLengthIs7.test("Hello")).isFalse();
        assertThat(startsWithJAndLengthIs7.test("HelloJ1")).isFalse();
        assertThat(startsWithJAndLengthIs7.test("Java1")).isFalse();
        assertThat(startsWithJAndLengthIs7.test("JavaOne")).isTrue();
    }
    // Hint:
    // <editor-fold defaultstate="collapsed">
    // see java.util.function.Predicate.and()
    // </editor-fold>

    /**
     * Given the two predicates p1 and p2, write a predicate that
     * returns true if the tested string is of length 4, true if
     * it starts with a J, but false if it is of length 4 and starts
     * with a J. This is a XOR operation on the predicates p1 and p2.
     */
    @Test
    public void a_predicate6() {
        Predicate<String> p1 = s -> s.length() == 4;
        Predicate<String> p2 = s -> s.startsWith("J");

        Predicate<String> p3 = null; //TODO

        assertThat(p3.test("True")).isTrue();
        assertThat(p3.test("Julia")).isTrue();
        assertThat(p3.test("Java")).isFalse();
    }

    /**
     * Create a predicate that is true if the length of the provided string
     * is 9 or the provided string equals ERROR.
     */
    @Test
    public void a_predicate7() {
        Predicate<String> lengthIs9 = s -> s.length() == 9;
        Predicate<String> equalsError = "ERROR"::equals;
        // Note: this could also be: Predicate.isEqual("ERROR")

        Predicate<String> lengthIs9orError = null; //TODO

        assertThat(lengthIs9orError.test("Hello")).isFalse();
        assertThat(lengthIs9orError.test("Hello J1!")).isTrue();
        assertThat(lengthIs9orError.test("ERROR")).isTrue();
        assertThat(lengthIs9orError.test("Error")).isFalse();
    }
    // Hint:
    // <editor-fold defaultstate="collapsed">
    // see java.util.function.Predicate.or()
    // </editor-fold>

    /**
     * Write a lambda expression that wraps the given
     * string in parentheses.
     */
    @Test
    public void b_function1() {
        Function<String, String> func = null; //TODO

        assertThat(func.apply("abc")).isEqualTo("(abc)");
    }

    /**
     * Write a lambda expression that converts the
     * given string to upper case.
     */
    @Test
    public void b_function2() {
        Function<String, String> func = null; //TODO

        assertThat(func.apply("abc")).isEqualTo("ABC");
    }

    /**
     * Write an unbound method reference that converts the
     * given string to upper case.
     */
    @Test
    public void b_function3() {
        Function<String, String> func = null; // TODO

        assertThat(func.apply("abc")).isEqualTo("ABC");
    }

    /**
     * Given two Functions, one that converts a null reference to an
     * empty string, and another that gets the length of a string,
     * create a single function converts nulls and then gets the
     * string's length.
     */
    @Test
    public void b_function4() {
        Function<String, String> unNullify = s -> s == null ? "" : s;
        Function<String, Integer> length = String::length;

        Function<String, Integer> lengthBis = null; //TODO

        assertThat(lengthBis.apply("Hello JavaOne!")).isEqualTo((Integer) 14);
        assertThat(lengthBis.apply("")).isEqualTo((Integer) 0);
        assertThat(lengthBis.apply(null)).isEqualTo((Integer) 0);
    }
    // Hint:
    // <editor-fold defaultstate="collapsed">
    // See java.util.Function.andThen() or java.util.Function.compose()
    // </editor-fold>

    /**
     * Write a lambda expression that appends the
     * string "abc" to the given StringBuilder.
     */
    @Test
    public void c_consumer1() {
        Consumer<StringBuilder> cons = null; //TODO

        StringBuilder sb = new StringBuilder("xyz");
        cons.accept(sb);

        assertThat(sb.toString()).isEqualTo("xyzabc");
    }

    /**
     * Write a lambda expression that clears the given list.
     */
    @Test
    public void c_consumer2() {
        Consumer<List<String>> cons = null; //TODO

        List<String> list = new ArrayList<>(List.of("a", "b", "c"));
        cons.accept(list);

        assertThat(list.isEmpty()).isTrue();
    }

    /**
     * Write an unbound method reference that clears the given list.
     */
    @Test
    public void c_consumer3() {
        Consumer<List<String>> cons = null; //TODO

        List<String> list = new ArrayList<>(List.of("a", "b", "c"));
        cons.accept(list);

        assertThat(list.isEmpty()).isTrue();
    }

    /**
     * Given two consumers, create a consumer that passes the String to the
     * first consumer, then to the second.
     */
    @Test
    public void c_consumer4() {
        Consumer<List<String>> c1 = list -> list.add("first");
        Consumer<List<String>> c2 = list -> list.add("second");

        Consumer<List<String>> consumer = null; //TODO

        List<String> list = new ArrayList<>(List.of("a", "b", "c"));
        consumer.accept(list);

        assertThat(List.of("a", "b", "c", "first", "second")).isEqualTo(list);
    }
    // Hint:
    // <editor-fold defaultstate="collapsed">
    // see java.util.function.Consumer.andThen()
    // </editor-fold>

    /**
     * Write a lambda expression that returns a new StringBuilder
     * containing the string "abc".
     */
    @Test
    public void d_supplier1() {
        Supplier<StringBuilder> sup = null; //TODO

        assertThat(sup.get().toString()).isEqualTo("abc");
    }

    /**
     * Write a lambda expression that returns a new, empty StringBuilder.
     */
    @Test
    public void d_supplier2() {
        Supplier<StringBuilder> sup = null; //TODO

        assertThat(sup.get().toString()).isEqualTo("");
    }

    /**
     * Write a constructor reference that returns a new, empty StringBuilder.
     */
    @Test
    public void d_supplier3() {
        Supplier<StringBuilder> sup = null; // TODO

        assertThat(sup.get().toString()).isEqualTo("");
    }

    /**
     * Write a lambda expression that, given two strings, returns the result
     * of concatenating the first with the second, followed by the
     * first again.
     */
    @Test
    public void e_bifunction1() {
        BiFunction<String, String, String> bifunc = null; //TODO

        assertThat(bifunc.apply("First", "Second")).isEqualTo("FirstSecondFirst");
    }

    /**
     * Write a lambda expression that returns the index of
     * the first occurrence of the second string within the first string,
     * or -1 if the second string doesn't occur within the first string.
     */
    @Test
    public void e_bifunction2() {
        BiFunction<String, String, Integer> bifunc = null; //TODO

        assertThat(bifunc.apply("abcdefghi", "def").intValue()).isEqualTo(3);
        assertThat(bifunc.apply("abcdefghi", "xyz").intValue()).isEqualTo(-1);
    }
    // Hint:
    // <editor-fold defaultstate="collapsed">
    // The String method
    //     public int indexOf(String)
    // works as a BiFunction, because the receiver (a String instance)
    // counts as the first argument. The argument to indexOf() becomes
    // the second argument to the BiFunction.
    // </editor-fold>

    /**
     * Write an unbound method reference that returns the index of
     * the first occurrence of the second string within the first string,
     * or -1 if the second string doesn't occur within the first string.
     */
    @Test
    public void e_bifunction3() {
        BiFunction<String, String, Integer> bifunc = null; // TODO

        assertThat(bifunc.apply("abcdefghij", "def").intValue()).isEqualTo(3);
        assertThat(bifunc.apply("abcdefghij", "xyz").intValue()).isEqualTo(-1);
    }
    // Hint 1:
    // <editor-fold defaultstate="collapsed">
    // Try using the IDE command to convert the lambda from the previous
    // exercise into a method reference.
    // </editor-fold>
    // Hint 2:
    // <editor-fold defaultstate="collapsed">
    // This is just like the example above with the argument shifting.
    // The only difference is that arguments aren't specified in a
    // method reference, so overload resolution has to do more work
    // to find the overloaded method that matches.
    // </editor-fold>


    /**
     * Write a lambda expression that appends the 'suffix'
     * variable (a String) to the sb variable (a StringBuilder).
     */
    @Test
    public void f_runnable1() {
        StringBuilder sb = new StringBuilder("abc");
        String suffix = "xyz";

        Runnable r = null; //TODO

        r.run();
        r.run();
        r.run();

        assertThat(sb.toString()).isEqualTo("abcxyzxyzxyz");
    }

    /**
     * Write a lambda expression that takes a string argument
     * and returns the index of that argument into the string
     * "abcdefghij", or that returns -1 if the string argument
     * doesn't occur.
     */
    @Test
    public void g_boundMethodRef1() {
        Function<String, Integer> func = null; //TODO

        assertThat(func.apply("cde").intValue()).isEqualTo(2);
        assertThat(func.apply("efg").intValue()).isEqualTo(4);
        assertThat(func.apply("xyz").intValue()).isEqualTo(-1);
    }
    // Hint:
    // <editor-fold defaultstate="collapsed">
    // Call the indexOf() method on a string literal.
    // </editor-fold>

    /**
     * Write a bound method reference that takes a string argument
     * and returns the index of that argument into the string
     * "abcdefghij", or that returns -1 if the string argument
     * doesn't occur. A bound method reference has an instance,
     * or an expression that evaluates to an instance, on the left-hand
     * side of the :: operator:
     * <p>
     * myObject::methodname
     * <p>
     * This is in contrast to an unbound method reference, which has
     * a classname on the left-hand side of the :: operator.
     */
    @Test
    public void g_boundMethodRef2() {
        Function<String, Integer> func = null; // TODO

        assertThat(func.apply("cde").intValue()).isEqualTo(2);
        assertThat(func.apply("efg").intValue()).isEqualTo(4);
        assertThat(func.apply("xyz").intValue()).isEqualTo(-1);
    }
    // Hint:
    // <editor-fold defaultstate="collapsed">
    // Place a string literal on the left-hand side of the :: operator.
    // </editor-fold>
}

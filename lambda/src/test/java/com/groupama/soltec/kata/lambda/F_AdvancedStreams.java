package com.groupama.soltec.kata.lambda;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.Map.entry;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * This set of exercises covers advanced stream operations,
 * including grouping collectors, composition of collectors,
 * and customized collectors.
 */
public class F_AdvancedStreams {
    @Test
    public void f1_mapInitialToWord() {
        List<String> alphabet = List.of("alfa", "bravo", "charlie", "delta");

        Map<String, String> result = null; //TODO;

        assertThat(result).containsExactly(
            Map.entry("a", "alfa"),
            Map.entry("b", "bravo"),
            Map.entry("c", "charlie"),
            Map.entry("d", "delta")
        );
    }

    @Test
    public void f2_mapFirstLetterToFirstLine() {
        Map<String, String> result = null; //TODO

        assertThat(result.size()).isEqualTo(8);
        assertThat(result).contains(
            Map.entry("P", "Pity the world, or else this glutton be,"),
            Map.entry("A", "And only herald to the gaudy spring,"),
            Map.entry("B", "But as the riper should by time decease,"),
            Map.entry("T", "That thereby beauty's rose might never die,"),
            Map.entry("F", "From fairest creatures we desire increase,"),
            Map.entry("W", "Within thine own bud buriest thy content,"),
            Map.entry("H", "His tender heir might bear his memory:"),
            Map.entry("M", "Making a famine where abundance lies,")
        );
    }

    @Test
    public void f3_mapFirstLetterToLastLine() {
        Map<String, String> result = null; //TODO

        assertThat(result.size()).isEqualTo(8);
        assertThat(result).contains(
            Map.entry("P", "Pity the world, or else this glutton be,"),
            Map.entry("A", "And, tender churl, mak'st waste in niggarding:"),
            Map.entry("B", "But thou contracted to thine own bright eyes,"),
            Map.entry("T", "To eat the world's due, by the grave and thee."),
            Map.entry("F", "Feed'st thy light's flame with self-substantial fuel,"),
            Map.entry("W", "Within thine own bud buriest thy content,"),
            Map.entry("H", "His tender heir might bear his memory:"),
            Map.entry("M", "Making a famine where abundance lies,")
        );
    }

    /**
     * Create a map from the lines of the sonnet,
     * with map keys being the first letter of the line, and values being the line.
     * For duplicate keys, concatenate the lines with a newline in between.
     */
    @Test
    public void f4_mapFirstLetterToAllLines() {
        Map<String, String> result = null; //TODO

        assertThat(result.size()).isEqualTo(8);
        assertThat(result).contains(
            Map.entry("P", "Pity the world, or else this glutton be,"),
            Map.entry(
                "A",
                "And only herald to the gaudy spring,\nAnd, tender churl, mak'st waste in niggarding:"
            ),
            Map.entry(
                "B",
                "But as the riper should by time decease,\nBut thou contracted to thine own bright eyes,"
            ),
            Map.entry(
                "T",
                "That thereby beauty's rose might never die,\n" +
                    "Thy self thy foe, to thy sweet self too cruel:\n" +
                    "Thou that art now the world's fresh ornament,\n" +
                    "To eat the world's due, by the grave and thee."
            ),
            Map.entry(
                "F",
                "From fairest creatures we desire increase,\nFeed'st thy light's flame with self-substantial fuel,"
            ),
            Map.entry("W", "Within thine own bud buriest thy content,"),
            Map.entry("H", "His tender heir might bear his memory:"),
            Map.entry("M", "Making a famine where abundance lies,")
        );
    }

    /**
     * Categorize the words from the text file into a map, where the map's key
     * is the length of each word, and the value corresponding to a key is a
     * list of words of that length. Don't bother with uniqueness or lower-
     * casing the words. As before, use the BufferedReader variable named
     * "reader" that has been set up for you to read from the text file, and
     * use SPLIT_PATTERN for splitting the line into words.
     *
     * @throws IOException
     */
    @Test
    public void f5_mapLengthToWordList() throws IOException {
        Map<Integer, List<String>> result = null; // TODO

        assertThat(result.get(7).size()).isEqualTo(10);
        assertThat(new HashSet<>(result.get(8))).isEqualTo(Set.of("beauty's", "increase", "ornament"));
        assertThat(new HashSet<>(result.get(9))).isEqualTo(Set.of("abundance", "creatures"));
        assertThat(new HashSet<>(result.get(10))).isEqualTo(Set.of("contracted", "niggarding"));
        assertThat(result.get(11)).isEqualTo(List.of("substantial"));
        assertThat(result.containsKey(12)).isFalse();
    }
    // Hint:
    // <editor-fold defaultstate="collapsed">
    // Use Collectors.groupingBy().
    // </editor-fold>

    /**
     * Collect the lines of the sonnet into a map,
     * whose keys are the first letter (as a String) of each line,
     * and whose values are a list of lines beginning with that letter.
     */
    @Test
    public void f6_mapFirstLetterToListOfLines() {
        Map<String, List<String>> result = null; //TODO


        assertThat(result.size()).isEqualTo(8);
        assertThat(result).contains(
            Map.entry("P", List.of("Pity the world, or else this glutton be,")),
            Map.entry(
                "A",
                List.of("And only herald to the gaudy spring,", "And, tender churl, mak'st waste in niggarding:")
            ),
            Map.entry(
                "B",
                List.of("But as the riper should by time decease,", "But thou contracted to thine own bright eyes,")
            ),
            Map.entry(
                "T",
                List.of(
                    "That thereby beauty's rose might never die,",
                    "Thy self thy foe, to thy sweet self too cruel:",
                    "Thou that art now the world's fresh ornament,",
                    "To eat the world's due, by the grave and thee."
                )
            ),
            Map.entry(
                "F",
                List.of(
                    "From fairest creatures we desire increase,",
                    "Feed'st thy light's flame with self-substantial fuel,"
                )
            ),
            Map.entry("W", List.of("Within thine own bud buriest thy content,")),
            Map.entry("H", List.of("His tender heir might bear his memory:")),
            Map.entry("M", List.of("Making a famine where abundance lies,"))
        );
    }


    /**
     * Categorize the words from the text file into a map, where the map's key
     * is the length of each word, and the value corresponding to a key is a
     * count of words of that length. Don't bother with uniqueness or lower-
     * casing the words. This is the same as the previous exercise except
     * the map values are the count of words instead of a list of words.
     *
     * @throws IOException
     */
    @Test
    public void f6_mapLengthToWordCount() throws IOException {
        Map<Integer, Long> result = null; // TODO

        assertThat(result).isEqualTo(Map.ofEntries(
            entry(1, 1L),
            entry(2, 11L),
            entry(3, 28L),
            entry(4, 21L),
            entry(5, 16L),
            entry(6, 12L),
            entry(7, 10L),
            entry(8, 3L),
            entry(9, 2L),
            entry(10, 2L),
            entry(11, 1L)
        ));
    }
    // Hint 1:
    // <editor-fold defaultstate="collapsed">
    // Use the overload of Collectors.groupingBy() that has
    // a "downstream" parameter.
    // </editor-fold>
    // Hint 2:
    // <editor-fold defaultstate="collapsed">
    // Use Collectors.counting().
    // </editor-fold>


    /**
     * Gather the words from the text file into a map, accumulating a count of
     * the number of occurrences of each word. Don't worry about upper case and
     * lower case. Extra challenge: implement two solutions, one that uses
     * groupingBy() and the other that uses toMap().
     *
     * @throws IOException
     */
    @Test
    public void f7_wordFrequencies() throws IOException {
        Map<String, Long> result = null; // TODO

        assertThat((long) result.get("tender")).isEqualTo(2L);
        assertThat((long) result.get("the")).isEqualTo(6L);
        assertThat((long) result.get("churl")).isEqualTo(1L);
        assertThat((long) result.get("thine")).isEqualTo(2L);
        assertThat((long) result.get("world")).isEqualTo(1L);
        assertThat((long) result.get("thy")).isEqualTo(4L);
        assertThat((long) result.get("self")).isEqualTo(3L);
        assertThat(result.containsKey("lambda")).isFalse();
    }
    // Hint 1:
    // <editor-fold defaultstate="collapsed">
    // For Collectors.groupingBy(), consider that each word needs to be in
    // a category of its own, that is, each word is categorized as itself.
    // </editor-fold>
    // Hint 2:
    // <editor-fold defaultstate="collapsed">
    // For Collectors.toMap(), the first occurrence of a word should be mapped to 1.
    // If two elements of the Stream are generating the same key, you will need to
    // provide a merging function.
    // </editor-fold>


    /**
     * From the words in the text file, create nested maps, where the outer map is a
     * map from the first letter of the word to an inner map. (Use a string of length
     * one as the key.) The inner map, in turn, is a mapping from the length of the
     * word to a list of words with that length. Don't bother with any lowercasing
     * or uniquifying of the words.
     * <p>
     * For example, given the words "foo bar baz bazz foo" the string
     * representation of the result would be:
     * {b={3=[bar, baz], 4=[bazz]}, f={3=[foo, foo]}}
     *
     * @throws IOException
     */
    @Test
    public void f8_nestedMaps() throws IOException {
        Map<String, Map<Integer, List<String>>> result = null; // TODO

        assertThat(result.get("a").get(9).toString()).isEqualTo("[abundance]");
        assertThat(result.get("b").get(2).toString()).isEqualTo("[by, be, by]");
        assertThat(result.get("f").get(5).toString()).isEqualTo("[flame, fresh]");
        assertThat(result.get("g").get(5).toString()).isEqualTo("[gaudy, grave]");
        assertThat(result.get("s").get(6).toString()).isEqualTo("[should, spring]");
        assertThat(result.get("s").get(11).toString()).isEqualTo("[substantial]");
        assertThat(result.get("t").get(3).toString())
            .isEqualTo("[the, thy, thy, thy, too, the, the, thy, the, the, the]");
        assertThat(result.get("w").get(5).toString()).isEqualTo("[where, waste, world]");
    }
    // Hint 1:
    // <editor-fold defaultstate="collapsed">
    // The nested map structure that's desired is the result of applying a
    // "downstream" collector that's the same operation as the first-level collector.
    // </editor-fold>
    // Hint 2:
    // <editor-fold defaultstate="collapsed">
    // Both collection operations are Collectors.groupingBy().
    // </editor-fold>


    /**
     * Given a stream of integers, compute separate sums of the even and odd values
     * in this stream. Since the input is a stream, this necessitates making a single
     * pass over the input.
     */
    @Test
    public void f9_separateOddEvenSums() {
        IntStream input = new Random(987523).ints(20, 0, 100);

        int sumEvens = 0; // TODO
        int sumOdds = 0; // TODO

        assertThat(sumEvens).isEqualTo(516);
        assertThat(sumOdds).isEqualTo(614);
    }
    // Hint 1:
    // <editor-fold defaultstate="collapsed">
    // Use Collectors.partitioningBy().
    // </editor-fold>
    // Hint 2:
    // <editor-fold defaultstate="collapsed">
    // The collect(Collector) method we need is defined on Stream<T>, but not on
    // IntStream, LongStream or DoubleStream.
    // </editor-fold>


    /**
     * Given a stream of strings, accumulate (collect) them into the result string
     * by inserting the input string at both the beginning and end. For example, given
     * input strings "x" and "y" the result should be "yxxy". Note: the input stream
     * is a parallel stream, so you MUST write a proper combiner function to get the
     * correct result.
     */
    @Test
    public void f10_insertBeginningAndEnd() {
        Stream<String> input = List.of(
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
            "k", "l", "m", "n", "o", "p", "q", "r", "s", "t")
            .parallelStream();

        String result = input.collect(null, null, null).toString();
        // TODO fill in lambda expressions or method references
        // in place of the nulls in the line above.

        assertThat(result).isEqualTo("tsrqponmlkjihgfedcbaabcdefghijklmnopqrst");
    }
    // Hint 1:
    // <editor-fold defaultstate="collapsed">
    // The collector state (that is, the object being accumulated and
    // combined) can be a single StringBuilder, which is manipulated
    // by lambda expressions in the three-arg form of the collect() method.
    // </editor-fold>
    // Hint 2:
    // <editor-fold defaultstate="collapsed">
    // The combiner function must take its second argument and merge
    // it into the first argument, mutating the first argument.
    // </editor-fold>
    // Hint 3:
    // <editor-fold defaultstate="collapsed">
    // The second argument to the combiner function happens AFTER the first
    // argument in encounter order, so the second argument needs to be split
    // in half and prepended/appended to the first argument.
    // </editor-fold>

    /**
     * Count the total number of words and the number of distinct, lower case
     * words in a stream, in one pass. This exercise uses a helper class
     * that defines methods that are called by the Stream.collect() method.
     * Your task is to fill in the implementation of the accumulate() and
     * combine() methods in the helper class. You don't need to modify the
     * test method itself.
     * <p>
     * The stream is run in parallel, so you must write a combine() method
     * that works properly.
     */
    static class TotalAndDistinct {
        private int count = 0;
        private final Set<String> set = new HashSet<>();

        // rely on implicit no-arg constructor

        void accumulate(String s) {
            // TODO write code to accumulate a single string into this object
        }

        void combine(TotalAndDistinct other) {
            // TODO write code to combine the other object into this one
        }

        int getTotalCount() {
            return count;
        }

        int getDistinctCount() {
            return set.size();
        }
    }
    // Hint:
    // <editor-fold defaultstate="collapsed">
    // The operations you need to write are actually quite simple.
    // Don't overthink it.
    // </editor-fold>

    @Test
    public void f11_countTotalAndDistinctWords() {
        List<String> allWords = reader.lines()
            .map(String::toLowerCase)
            .flatMap(line -> SPLIT_PATTERN.splitAsStream(line))
            .collect(Collectors.toList());

        TotalAndDistinct totalAndDistinct =
            Collections.nCopies(100, allWords)
                .parallelStream()
                .flatMap(List::stream)
                .collect(TotalAndDistinct::new,
                    TotalAndDistinct::accumulate,
                    TotalAndDistinct::combine);

        assertThat(totalAndDistinct.getDistinctCount()).withFailMessage("distinct count").isEqualTo(81);
        assertThat(totalAndDistinct.getTotalCount()).withFailMessage("total count").isEqualTo(10700);
    }

    // ========================================================
    // END OF EXERCISES
    // TEST INFRASTRUCTURE IS BELOW
    // ========================================================


    // Pattern for splitting a string into words
    static final Pattern SPLIT_PATTERN = Pattern.compile("[- .:,]+");

    private BufferedReader reader;

    @BeforeEach
    public void z_setUpBufferedReader() throws IOException, URISyntaxException {
        reader = Files.newBufferedReader(Paths.get(ClassLoader.getSystemResource("SonnetI.txt").toURI()), StandardCharsets.UTF_8);
    }

    @AfterEach
    public void z_closeBufferedReader() throws IOException {
        reader.close();
    }
}

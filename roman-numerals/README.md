# Roman Numerals

### Converting Arabic to Roman
* 1 &rarr; I
* 5 &rarr; V
* 10 &rarr; X
* 50 &rarr; L
* 100 &rarr; C
* 500 &rarr; D
* 1000 &rarr; M

### Converting Roman to Arabic

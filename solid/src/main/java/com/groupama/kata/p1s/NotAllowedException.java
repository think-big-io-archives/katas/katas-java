package com.groupama.kata.p1s;

public class NotAllowedException extends Exception {
    public NotAllowedException(String message) {
        super(message);
    }
}

package com.groupama.kata.p1s;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Settings {
    private String email;
    private String favoriteColor;
}

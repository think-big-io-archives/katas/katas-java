package com.groupama.kata.p1s;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class User {
    private String name;
    private List<String> rights;
}

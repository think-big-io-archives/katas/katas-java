package com.groupama.kata.p1s;

import java.util.HashMap;
import java.util.Map;

public class UserSettingsService {

    public static final String RIGHT_UPDATE_DATA = "UpdateSettings";

    private Map<String, Settings> settings = new HashMap<>();

    public boolean changeSettings(User user, Settings settings) {
        if (checkAccess(user)) {
            this.settings.put(user.getName(), settings);
            return true;
        }
        return false;
    }

    public Settings getSettings(User user) {
        return settings.get(user.getName());
    }

    public boolean checkAccess(User user) {
        return user.getRights().contains(RIGHT_UPDATE_DATA);
    }
}

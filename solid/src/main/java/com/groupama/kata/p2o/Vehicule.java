package com.groupama.kata.p2o;

import com.groupama.kata.p2o.moteur.MoteurDiesel;
import com.groupama.kata.p2o.moteur.MoteurEssence;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Vehicule {

    private MoteurType moteurType;

    public Vehicule(MoteurType moteurType) {
        this.moteurType = moteurType;
    }

    public boolean start() {
        switch (moteurType) {
            case DIESEL:
                MoteurDiesel moteurDiesel = new MoteurDiesel();
                return moteurDiesel.prechauffageDemarrage();
            case ESSENCE:
                MoteurEssence moteurEssence = new MoteurEssence();
                return moteurEssence.demarrage();
            default:
        }

        return false;
    }
}

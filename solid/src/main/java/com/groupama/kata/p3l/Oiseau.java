package com.groupama.kata.p3l;


public abstract class Oiseau {

    private int latitude;
    private int longitude;
    private int altitude;

    public abstract String getName();

    public int[] getPosition() {
        return new int[] { latitude, longitude};
    }

    public void setPosition(int latitude, int longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getAltitude() {
        return altitude;
    }

    public void setAltitude(int altitude) {
        this.altitude = altitude;
    }
}

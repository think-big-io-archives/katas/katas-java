package com.groupama.kata.p5d;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DevJava {

    public void boitCafe() {
        log.info("Café bu");
    }

    public void codeJava() {
        log.info("Itération Java ok");
    }
}

package com.groupama.kata.p5d;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DevJavascript {
    public void codeJavascript() {
        log.info("Itération Javascript ok");
    }
}

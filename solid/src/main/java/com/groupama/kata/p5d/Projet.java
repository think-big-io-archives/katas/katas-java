package com.groupama.kata.p5d;

public class Projet {

    private DevJava devJava;
    private DevJavascript devJavascript;
    private int iteration;

    public Projet() {
        devJava = new DevJava();
        devJavascript = new DevJavascript();
    }

    public void runIteration() {
        devJava.codeJava();
        devJavascript.codeJavascript();
        iteration++;
    }

    public int getIteration() {
        return iteration;
    }
}

# String Calculator Kata

1. Create a simple String calculator with a method <code>int add(String numbers)</code>
    * The method can take 0, 1 or 2 numbers
    * Start with the simplest test case of an empty string and move to 1 and 2 numbers
    * Solve things as simply as possible so that you force yourself to write tests you did not think about
    * Remember to refactor after each test
1. Allow the <code>add()</code> method to handle an unknown amount of numbers
1. Allow the <code>add()</code> method to handle new lines between numbers instead of commas
    * The following input is ok: <code>1\n2,3</code>, the result is 6
    * The following input is not ok: <code>1,\n</code> (no need to prove it)
1. Support different delimiters
    * To change a delimiter, the beginning of the string will contain a separate line that looks like this: <code>//[delimiter]\n[numbers...]</code> for example <code>//;\n1;2</code> should return three where the default delimiter is <code>;</code>
    * The first line is optional. All existing scenarios should still be supported.
1. Call <code>add()</code> with a negative number will throw an exception <code>negatives not allowed</code> and the negative that was passed. If there are multiple negatives, show all of them in the exception message.
1. Numbers bigger than 1000 should be ignored, so adding 2 + 1001 = 2 

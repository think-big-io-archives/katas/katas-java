package com.groupama.soltec.kata.telldontask.usecase;

import com.groupama.soltec.kata.telldontask.domain.Order;
import com.groupama.soltec.kata.telldontask.repository.OrderRepository;
import com.groupama.soltec.kata.telldontask.service.ShipmentService;

import static com.groupama.soltec.kata.telldontask.domain.OrderStatus.*;

public class OrderShipmentUseCase {

    private final OrderRepository orderRepository;
    private final ShipmentService shipmentService;

    public OrderShipmentUseCase(OrderRepository orderRepository, ShipmentService shipmentService) {
        this.orderRepository = orderRepository;
        this.shipmentService = shipmentService;
    }

    public void run(OrderShipmentRequest request) {
        final Order order = orderRepository.getById(request.getOrderId());

        if (order.getStatus().equals(CREATED) || order.getStatus().equals(REJECTED)) {
            throw new OrderCannotBeShippedException();
        }

        if (order.getStatus().equals(SHIPPED)) {
            throw new OrderCannotBeShippedTwiceException();
        }

        shipmentService.ship(order);

        order.setStatus(SHIPPED);
        orderRepository.save(order);
    }

}

Train Reservation Kata
======================

Railway operators aren't always known for their use of cutting edge technology,
and in this case they're a little behind the times.
The railway people want you to help them to improve their online booking service.
They'd like to be able to not only sell tickets online,
but to decide exactly which seats should be reserved,
at the time of booking.

You're working on the "TicketOffice" service,
and your next task is to implement the feature for reserving seats on a particular train.
The railway operator has a service-oriented architecture, and both the interface you'll need to fulfill,
and some services you'll need to use are already implemented.

## Business Rules around Reservations

There are various business rules and policies around which seats may be reserved.
For a train overall, no more than 70% of seats may be reserved in advance,
and ideally no individual coach should have no more than 70% reserved seats either.
However, there is another business rule that says you _must_ put all the seats for one reservation in the same coach.
This could make you and go over 70% for some coaches, just make sure to keep to 70% for the whole train.

## The Guiding Test

The Ticket Office service needs to respond to a HTTP POST request that comes with form data telling you which train the customer wants to reserve seats on,
and how many they want. It should return a json document detailing the reservation that has been made. 

A reservation comprises a json document with three fields,
the train id, booking reference, and the ids of the seats that have been reserved. Example json:

	{"train_id": "express_2000", "booking_reference": "75bcd15", "seats": ["1A", "1B"]}
	
### Booking Reference Service

You can get a unique booking reference using a REST-based service.
For test purposes, you can start a local service using the provided code in the _com.groupama.soltec.kata.trainreservation.external.bookingreference.BookingReferenceService_ class.
You can assume the real service will behave the same way, but be available on a different url.


You can use this service to get a unique booking reference. Make a GET request to:

    http://localhost:8082/booking_reference

This will return a string that looks a bit like this:

	75bcd15

### Train Data Service 

You can get information about which each train has by using the train data service.
For test purposes, you can start a local service using the provided code in the _com.groupama.soltec.kata.trainreservation.external.traindata.TrainDataService_ class.
You can assume the real service will behave the same way, but be available on a different url.

You can use this service to get data for example about the train with id "express_2000" like this:

    http://localhost:8081/data_for_train/express_2000

this will return a json document with information about the seats that this train has. The document you get back will look for example like this:

    {"seats": {"1A": {"booking_reference": "", "seat_number": "1", "coach": "A"}, "2A": {"booking_reference": "", "seat_number": "2", "coach": "A"}}}

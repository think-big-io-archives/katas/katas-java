package com.groupama.soltec.kata.trainreservation.external.traindata.model;

import com.groupama.soltec.kata.trainreservation.external.traindata.exception.SeatNotEmptyException;
import com.groupama.soltec.kata.trainreservation.external.traindata.exception.SeatNotInTrainException;

import java.util.List;
import java.util.stream.Collectors;

public class TrainData {
    private final String trainId;
    private final List<TrainDataSeat> seats;

    public TrainData(String trainId, List<TrainDataSeat> seats) {
        this.trainId = trainId;
        this.seats = seats;
    }

    public String id() {
        return trainId;
    }

    public List<TrainDataSeat> seats() {
        return seats;
    }

    public void reserve(List<String> seatIds, String bookingReference) {
        List<String> notInTrainSeats = findSeatsNotInTrain(seatIds);
        if (!notInTrainSeats.isEmpty()) {
            throw new SeatNotInTrainException(notInTrainSeats.get(0));
        }

        List<String> alreadyBookedSeats = findAlreadyBookedSeats(seatIds);
        if (!alreadyBookedSeats.isEmpty()) {
            throw new SeatNotEmptyException(alreadyBookedSeats.get(0));
        }

        seats.forEach(seat -> {
            if (seatIds.contains(seat.id())) {
                seat.reserve(bookingReference);
            }
        });
    }

    private List<String> findSeatsNotInTrain(List<String> seatsIds) {
        List<String> trainSeatIds = seats.stream().map(TrainDataSeat::id).collect(Collectors.toList());
        return seatsIds.stream().filter(id -> !trainSeatIds.contains(id)).collect(Collectors.toList());
    }

    private List<String> findAlreadyBookedSeats(List<String> seatIds) {
        List<String> bookedSeatIds = seats.stream().filter(TrainDataSeat::isBooked).map(TrainDataSeat::id).collect(Collectors.toList());
        return seatIds.stream().filter(bookedSeatIds::contains).collect(Collectors.toList());
    }

    public void reset() {
        seats.forEach(TrainDataSeat::reset);
    }
}

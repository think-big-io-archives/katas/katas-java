package com.groupama.soltec.kata.trainreservation.external.traindata.model;

public class TrainDataSeat {
    private String seatId;
    private String coach;
    private String seatNumber;
    private String bookingReference;

    public TrainDataSeat(String seatId, String seatNumber, String bookingReference, String coach) {
        this.seatId = seatId;
        this.seatNumber = seatNumber;
        this.bookingReference = bookingReference;
        this.coach = coach;
    }

    public String id() {
        return seatId;
    }

    public String number() {
        return seatNumber;
    }

    public String bookingReference() {
        return bookingReference;
    }

    public String coach() {
        return coach;
    }

    public void reserve(String bookingReference) {
        this.bookingReference = bookingReference;
    }

    public void reset() {
        bookingReference = "";
    }

    public boolean isBooked() {
        return bookingReference != null && bookingReference.length() > 0;
    }
}

package com.groupama.soltec.kata.trainreservation.legacy.core;

import java.util.ArrayList;
import java.util.List;

public class TrainJson {
    public List<SeatJson> seats;

    public TrainJson() {
        this.seats = new ArrayList<SeatJson>();
    }
}

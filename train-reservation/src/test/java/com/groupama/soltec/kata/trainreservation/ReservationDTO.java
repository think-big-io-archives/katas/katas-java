package com.groupama.soltec.kata.trainreservation;

import java.util.List;

public class ReservationDTO {

    public String train_id;
    public String booking_reference;
    public List<String> seats;

    @Override
    public String toString() {
        return "ReservationDTO{" +
            "train_id='" + train_id + '\'' +
            ", booking_reference='" + booking_reference + '\'' +
            ", seats=" + seats +
            '}';
    }
}

package com.groupama.soltec.kata.tripservice.trip;

import com.groupama.soltec.kata.tripservice.exception.CollaboratorCallException;
import com.groupama.soltec.kata.tripservice.user.User;

import java.util.List;

public class TripDAO {

    public static List<Trip> findTripsByUser(User user) {
        throw new CollaboratorCallException("TripDAO should not be invoked on an unit test.");
    }

}

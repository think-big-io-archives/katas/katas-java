package com.groupama.soltec.kata.tripservice.trip;

import com.groupama.soltec.kata.tripservice.exception.UserNotLoggedInException;
import com.groupama.soltec.kata.tripservice.user.User;
import com.groupama.soltec.kata.tripservice.user.UserSession;

import java.util.ArrayList;
import java.util.List;

public class TripService {

    public List<Trip> getTripsByUser(User user) throws UserNotLoggedInException {
        List<Trip> list = new ArrayList<Trip>();
        User other = UserSession.getInstance().getLoggedUser();
        boolean isFriend = false;
        if (other != null) {
            for (User u : user.getFriends()) {
                if (u.equals(other)) {
                    isFriend = true;
                    break;
                }
            }
            if (isFriend) {
                list = TripDAO.findTripsByUser(user);
            }
            return list;
        } else {
            throw new UserNotLoggedInException();
        }
    }

}
